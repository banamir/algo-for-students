import java.util.*;

public class UndirectedGraph {

    private int E,V;
    Set<Integer>[] adjacent;

    UndirectedGraph(int V){
        this.V = V;
        adjacent = new Set[V];
        for(int i = 0; i < adjacent.length; i++){
            adjacent[i] = new HashSet<Integer>();
        }
    }

    public int verticesCount(){
        return V;
    }

    public int edgesCount(){
        return E;
    }

    public void add(int u, int v){
        validate(u);
        validate(v);

        if(!adjacent[u].contains(v)){
            E++;
            adjacent[u].add(v);
            adjacent[v].add(u);

        }
    }

    public Integer[] adj(int v){
        validate(v);
        Integer[] vertices = new Integer[adjacent[v].size()];
        return adjacent[v].toArray(vertices);
    }



    private void validate(int v){
        if(v >= V || v < 0) throw new IllegalArgumentException();
    }

    public static UndirectedGraph readGraph(){
        Scanner in  = new Scanner(System.in);
        int V = in.nextInt(), E = in.nextInt();
        UndirectedGraph graph = new UndirectedGraph(V);
        for(int i = 0; i < V; i++){
            graph.add(in.nextInt(), in.nextInt());
        }
        return graph;
    }

    public static void main(String args[]){

        UndirectedGraph graph = new UndirectedGraph(10);

        graph.add(1,5);
        graph.add(2,6);
        graph.add(4,2);
        graph.add(3,4);
        graph.add(4,6);

        System.out.println(graph.verticesCount());
        System.out.println(graph.edgesCount());
        graph.add(4,3);
        System.out.println(graph.edgesCount());

        for(int v : graph.adj(2)){
            System.out.printf("%d ", v);
        }
    }
}
