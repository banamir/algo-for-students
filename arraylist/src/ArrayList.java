import java.util.Iterator;

public class ArrayList implements List<Integer> {

    private Integer [] values;

    private int size;



    public ArrayList() {

        size = 0;
        values = new Integer[1];


    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void remove(int index) {

        for(int i = index; i< size -1; i++) {
            values[i] = values[i + 1];
        }
        size--;
        if(size < values.length/2){
            resize(3*values.length/4);
        }
    }

    @Override
    public void add(int ind, Integer element) {

    }

    @Override
    public void add(Integer element) {

        if(size == values.length) {
            resize (size *2);
        }
        size++;

        values[size -1] = element;


    }
    private void resize (int newsize) {
        Integer [] newValues = new Integer[newsize];

        for(int i = 0; i< size; i++) {
            newValues[i]= values[i];
        }

        values = newValues;
    }

    @Override
    public Integer get(int index) {
        return values[index];
    }

    @Override
    public void set(int ind, Integer element) {
        values[ind] = element;
    }

    @Override
    public Iterator<Integer> iterator() {
        return null;
    }

    public static void main(String[] args) {

        List<Integer> list  = new ArrayList();

        list.add(5);
        list.add(8);
        list.add(10);
        list.add(1);

        System.out.println(list.size());
        for(int i = 0; i <  list.size(); i++){
            System.out.println(list.get(i));
        }

        list.remove(2);
        System.out.println(list.size());
        for(int i = 0; i <  list.size(); i++){
            System.out.println(list.get(i));
        }
    }
}
