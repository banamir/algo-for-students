
public interface Queue<E> extends Iterable<E> {

    /**
     * Get size of the stack
     *
     * @return the size of elemnts
     */
    int size();

    /**
     * Check if the list is empty
     *
     * @return true if empty else false
     */
    boolean isEmpty();

    /**
     * Add element to the queue
     *
     * @param element - element to be added
     */
    void enqueue(E element);

    /**
     * Retrive the element from  the queue.
     * After retrieving the element is removed from the queue.
     *
     * @return - retrieved element
     */
    E dequeue();

}
