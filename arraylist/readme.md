# ArrayLists

##### Official documentaion
    
- [java.util](https://docs.oracle.com/javase/8/docs/api/java/util/package-summary.html)
- [List](https://docs.oracle.com/javase/8/docs/api/java/util/List.html)
- [ArrayList.java](https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html)
- [LinkedList.java](https://docs.oracle.com/javase/8/docs/api/java/util/LinkedList.html)
- [Collections.java](https://docs.oracle.com/javase/8/docs/api/java/util/Collections.html)

##### Docs

- [Array List](https://www.csee.umbc.edu/courses/undergraduate/202/fall11H/lectures/ArraysArrayList.pdf)  
- [Java Collections 1](http://www.quizful.net/post/Java-Collections)  
- [Java Collections 2](https://habrahabr.ru/post/237043/)  
- [Algorithm analysis](https://algs4.cs.princeton.edu/14analysis/)  

##### Implementation

- [ArrayQueue](https://algs4.cs.princeton.edu/13stacks/ResizingArrayQueue.java.html)  
- [ArrayStack](https://algs4.cs.princeton.edu/13stacks/ResizingArrayStack.java.html)   