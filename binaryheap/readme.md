#Binary Heap

##### Official documentaion
    
- [PriorityQueue](https://docs.oracle.com/javase/7/docs/api/java/util/PriorityQueue.html)


##### Docs

- [ Sedgewick and Kevin Wayne](https://algs4.cs.princeton.edu/24pq/)  
- [Sedgewick and Kevin Wayne (Presentation)](https://algs4.cs.princeton.edu/lectures/24PriorityQueues.pdf)  

##### Implementation

- [MaxPriorityQueue](https://algs4.cs.princeton.edu/24pq/MaxPQ.java.html)  
- [MinPriorityQueue](https://algs4.cs.princeton.edu/24pq/MinPQ.java.html)  