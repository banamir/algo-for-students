import java.util.ArrayList;
import java.util.PriorityQueue;

public class BinaryHeap {


    ArrayList<Integer> heap;

    BinaryHeap(){
        //was:
        //heap = new ArrayList<>(1);
        //MUST BE:
        heap = new ArrayList<>();
        heap.add(null);
    }



    int size(){
        return heap.size()-1;
    }

    boolean isEmpty(){
        return heap.size()==1;
    }

    void add(int element){
        heap.add(element);
        int i = heap.size()-1;
        //was:
//        while (heap.get(i/2) < heap.get(i)){
        //MUST BE:
        while (i != 1 && heap.get(i/2) > heap.get(i)){

            int temp = heap.get(i);
            heap.set(i, heap.get(i/2));
            heap.set(i/2, temp);

            i=i/2;
        }
    }

    int poll(){

        int min = heap.get(1);
        heap.set(1, heap.get(heap.size()-1));
        heap.remove(heap.size()-1);

        int i=1;


        while((2*i < heap.size() && heap.get(2*i)<heap.get(i)) ||
                (2*i + 1 < heap.size() && heap.get(2*i+1)<heap.get(i))){
//was:
//            if(heap.get(2*i)<heap.get(i)){
//
//                int temp = heap.get(i);
//                heap.set(i, heap.get(i*2));
//                heap.set(i*2, temp);
//
//                i=2*i;
//
//            }else if(heap.get(2*i+1)<heap.get(i)){
//
//                int temp = heap.get(i);
//                heap.set(i, heap.get(2*i + 1));
//                heap.set(2*i+1, temp);
//
//                i=2*i+1;
//            }
//MUST BE:
            int j = 2*i;
            if(j + 1 < heap.size() && heap.get(j) >  heap.get(j + 1)) j++;

            if(heap.get(j)<heap.get(i)){

                int temp = heap.get(i);
                heap.set(i, heap.get(j));
                heap.set(j, temp);

                i = j;
           }

        }
        return min;

    }

    int peek(){
        int min = heap.get(1);
        return min;

    }

    public static void main(String[] args) {

//        PriorityQueue<Integer> pq = new PriorityQueue<>();
        BinaryHeap pq = new BinaryHeap();

        pq.add(5);
        pq.add(4);
        pq.add(1);
        pq.add(8);
        pq.add(6);
        pq.add(24);
        pq.add(4);

        System.out.println(pq.peek());
        System.out.println();

        pq.poll();
        System.out.println(pq.peek());
        System.out.println();

        pq.add(3);
        System.out.println(pq.peek());
        System.out.println();

        pq.add(2);
        System.out.println(pq.peek());
        System.out.println();

        int size = pq.size();
        for(int i  = 0; i < size; i++){
            System.out.println(pq.poll());
        }
    }


}
